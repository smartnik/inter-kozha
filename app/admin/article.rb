ActiveAdmin.register Article do
  permit_params :title, :description, :text, :image, :meta_description

  index do
    selectable_column
    column :id
    column(:text){ |article| article.text.truncate(100) }
    column(:description){ |article| article.description.truncate(100) }
    column(:meta_description){ |article| article.meta_description.to_s.truncate(100) }
    column :image
    column :created_at
    column :updated_at

    actions
  end

  form(html: { multipart: true }) do |f|
    f.inputs "Article" do
      f.input :title
      f.input :text, as: :rich, config: {}
      f.input :description
      f.input :meta_description
      f.input :image, as: :file
    end

  f.actions
end


end
