ActiveAdmin.register Category do
  permit_params :name, :image, :position

  form(html: { multipart: true }) do |f|
    f.inputs 'Category' do
      f.input :name
      f.input :image, as: :file
      f.input :position
    end

    f.actions
  end
end
