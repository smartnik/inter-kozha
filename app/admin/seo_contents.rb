ActiveAdmin.register SeoContent do
  permit_params :title, :meta_description, :content, :address, :meta_keywords

  index do
    selectable_column
    column :id
    column :title
    column :meta_description
    column :meta_keywords
    column :content
    column :address

    actions
  end

  form(html: { multipart: true }) do |f|
    f.inputs "SeoContent" do
      f.input :title
      f.input :meta_description
      f.input :meta_keywords
      f.input :content, as: :rich, config: {}
      f.input :address
    end

    f.actions
  end
end
