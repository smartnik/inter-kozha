$( document ).ready(function() {
  $("#new_email_subscription").on("ajax:success", function(e, data, status, xhr) {
    $('.hollow-button.submit').addClass('disabled');
    $('footer p.mirta').fadeOut('fast', function() {
      $(this).text("Спасибо, что подписались!")
    }).fadeIn();
    return;
  }).on("ajax:error", function(e, data, status, xhr) {
    $(".email-subscribe").effect('shake', {times: 2}, 600);
    return;
  });

});
