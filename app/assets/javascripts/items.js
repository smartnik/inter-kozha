
$( document ).ready(function() {
  var image = $('#main-production-image');
  // image.elevateZoom({
  //   zoomType: "inner",
  //   cursor: "crosshair",
  //   scrollZoom: true
  // });

  $('.product-image-block').first().addClass('active');
  $('.product-image-block').click(function() {
    url = this.style.backgroundImage.replace(/"(|\))/g, '').replace('small', 'medium');
    url = url.replace('url(', '').replace(')', '')

    $('#main-production-image').attr('src', url);

    // $('.zoomContainer').remove();
    // $(image).removeData('elevateZoom');
    // $(image).removeData('zoomImage');

    // $("#main-production-image").elevateZoom({
    //   zoomType: "inner",
    //   cursor: "crosshair",
    //   scrollZoom: true
    // });

    $('.active.product-image-block').removeClass('active');
    this.classList.add('active');
  });

  $('#request-form').hide();
  $('#leave-request').click(function() {
    $('#request-form').show();
    $('#leave-request').hide();
    $('#request-form').fadeIn('fast');
  });

  $("#request-form").on("ajax:success", function(e, data, status, xhr) {
    $('#request-form').hide();
    $('#leave-request').fadeIn('fast');
    document.getElementById('leave-request').innerHTML = 'Спасибо! Мы скоро с вами свяжемся'
    return;
  }).on("ajax:error", function(e, data, status, xhr) {
    $('#request-form').hide();
    $('#leave-request').fadeIn('fast');
    document.getElementById('leave-request').innerHTML = 'Что-то пошло не так, попрубуйте еще раз';
  });

});
