class NotifyTelegram
  URL = 'https://api.telegram.org/bot345703244:AAGQQa8tpyUMKKpg3ZgliGMSfOUzwQWTmWY/sendMessage'
  CHAT_ID = 285313303

  attr_reader :request

  def initialize(request)
    @request = request
  end

  def call
    RestClient.post(URL, { text: text, chat_id: CHAT_ID })
  end

  private

  def text
    "#{request.request}\ncontact: #{request.contact}"
  end
end
