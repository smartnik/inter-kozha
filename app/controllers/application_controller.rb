class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def current_user
    return User.first
    # @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def seo_content
    @seo_content ||= SeoContent.find_by(address: request.fullpath)
  end

  helper_method :current_user, :seo_content

  def authorize
    return true
    redirect_to root_path if current_user.nil?
  end
end
