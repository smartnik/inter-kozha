class ArticlesController < ApplicationController
  before_filter :authorize, except: [:show, :index]

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(valid_params)
    @article.images = images_param
    @article.save
    redirect_to articles_path
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    @article.images = images_param
    @article.update!(valid_params)
    redirect_to @article
  end

  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
    render layout: 'full_width'
  end

  def destroy
    @article = Article.delete(params[:id])
    redirect_to :back
  end

  private

  def images_param
    params['article']['images']
  end

  def valid_params
    params.require(:article).permit(:text, :title, :image, :description, :meta_description)
  end
end
