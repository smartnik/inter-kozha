class EmailSubscriptionsController < ApplicationController
  before_filter :authorize, except: [:new, :create]

  def create
    @request = EmailSubscription.new(valid_params)
    if @request.save
      cookies[:email_subscribed] = @request.email
      head :created
    else
      head :bad_request
    end
  end

  def valid_params
    params.require(:email_subscription).permit(:email)
  end

end
