class ErrorsController < ApplicationController
  def show(code: 404)
    if code == 404
      render 'errors/show', status: 404
    else
      render file: "public/#{code}.html", status: code
    end
  end
end
