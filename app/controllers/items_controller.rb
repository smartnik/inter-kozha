class ItemsController < ApplicationController
  # before_filter :authorize, except: [:show, :index]

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(valid_params)
    @item.images = images_param
    if @item.save
      redirect_to @item
    else
      render 'new'
    end
  end

  def edit
    @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])
    @item.attributes = valid_params
    @item.images = images_param
    @item.save
    redirect_to @item
  end

  def index
    @optom = params[:of_kind] == 'Оптом'

    @items = if(kind = params[:of_kind])
      Item.of_kind kind
    else
      Item.all
    end.by_category_position.by_created

    render layout: 'full_width'
  end

  def show
    @item = Item.find(params[:id])
    @related_items = Item.of_kind(@item.kind).without(@item.id).last(3)
    @request = Request.new
  end

  def upload_image
    image = Upload.new image: params[:file]
    image.save
    render json: image.image.url
  end

  def destroy
    Item.delete(params[:id])
    redirect_to :back
  end

  private

  def valid_params
    params.
      require(:item).
      permit(:description, :title, :images, :price, :kind, :sizes, :featured, :discount)
  end

  def images_param
    params['item']['images']
  end
end
