class RequestsController < ApplicationController
  before_filter :authorize, except: [:new, :create]

  def new
    @request = Request.new
  end

  def index
    @requests = Request.all
  end

  def create
    @request = Request.new(valid_params)
    if @request.save
      NotifyTelegram.new(@request).call
      head :created
    else
      head :bad_request
    end
  end

  def valid_params
    params.require(:request).permit(:contact, :request)
  end
end
