class StaticPagesController < ApplicationController
  layout 'full_width'
  def index
    @related_items = Item.featured.last(3)
    @categories = Category.first(4)
    @slider = StaticPage.find_by(name: 'slider')
  end

  def about
    @about_us = StaticPage.find_by(name: 'About Us')
  end

  def contacts
    @request = Request.new
  end
end
