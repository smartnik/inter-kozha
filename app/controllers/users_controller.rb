class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(valid_params)
    if @user.save
      sesstion[:user_id] = @user.id
      redirect_to root_path, notice: "Спасибо за регистрацию"
    else
      render "new", notice: "Произошла ошибка, скорее всего вы уже зарегестрированны"
    end
  end

  private

  def valid_params
    params.require(:user).permit(:password, :email, :password_confirmation)
  end
end
