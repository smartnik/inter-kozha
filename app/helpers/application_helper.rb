module ApplicationHelper
  DEFAULT_META_DESCRIPTION = "Приветствуем вас в интернет-магазине дизайнерской обуви, рюкзаков, сумок и " \
    "других аксессуаров из натуральной кожи. Устали от банальных вещей? Здесь вы " \
    "однозначно найдете шикарные эксклюзивные изделия, соответствующие вашим " \
    "представлениям о стиле и красоте!".freeze

  def page_title(title= nil, add_extra: false)
    return seo_content.title if seo_content.present?
    @title ||= title || ''

    if add_extra
      "#{@title} Дизайнерская обувь и аксессуары из натуральной кожи в магазине 13РМ"
    else
      @title
    end
  end

  def meta_keywords
    return seo_content.meta_keywords if seo_content.present?
    "Дизайнерская обувь, сумки из натуральной кожи, кожанные рюкзаки"
  end

  def meta_description(description = nil)
    return seo_content.meta_description if seo_content.present?
    @meta_description ||= description || DEFAULT_META_DESCRIPTION
  end

  def item_title
    case params[:of_kind].to_s
    when '' then "Каталог кожи"
    when 'Оптом' then "Кожа оптом"
    else "Кожа для #{params[:of_kind].downcase}"
    end
  end
end
