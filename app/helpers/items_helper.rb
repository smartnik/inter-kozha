module ItemsHelper
  def leather_options
    Category.pluck(:name).map { |c| [c, c] }
  end
end
