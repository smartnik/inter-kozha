class ApplicationMailer < ActionMailer::Base
  default from: "request@leder-kradt.ru"
  layout 'mailer'
end
