class RequestMailer < ApplicationMailer
  def successful(request)
    @request = request
    mail to: 'misharinvm@gmail.com', subject: 'Новый запрос'
  end
end
