# == Schema Information
#
# Table name: articles
#
#  id          :integer          not null, primary key
#  title       :string
#  text        :text
#  views       :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  image       :string
#  description :text
#  images      :string           is an Array
#

class Article < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  mount_uploaders :images, ImageUploader
end
