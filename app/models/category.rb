class Category < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  acts_as_list

  scope :ordered, -> { order(position: :asc)}

  default_scope { ordered }
end
