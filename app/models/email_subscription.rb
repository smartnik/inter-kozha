# == Schema Information
#
# Table name: email_subscriptions
#
#  id    :integer          not null, primary key
#  email :string
#

class EmailSubscription < ActiveRecord::Base
  EMAIL_VALIDATION_REGEXP = /\A\w([\w+.=\-]*\w)?@\w([\w\-]*\.)+\w+\z/

  validates :email, format: { with: EMAIL_VALIDATION_REGEXP }
end
