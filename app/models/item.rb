# == Schema Information
#
# Table name: items
#
#  id          :integer          not null, primary key
#  title       :string
#  description :string
#  price       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  kind        :string
#  images      :string           is an Array
#  featured    :boolean
#

class Item < ActiveRecord::Base
  mount_uploaders :images, ImageUploader

  scope :of_kind, -> kind { where(kind: kind) }
  scope :without, -> id { where.not(id: id)}
  scope :by_created, -> { order(created_at: :desc) }
  scope :featured, -> { where(featured: true) }
  scope :by_category_position, -> { joins(:category).order('categories.position asc') }

  validates :discount, inclusion: { in: 0..99 }

  belongs_to :category

  before_save :update_category, if: -> { changed_attributes['kind'].present? }

  def price_with_discount
    (100 - discount) / 100.to_f * price.to_i
  end

  def update_category
    self.category = Category.find_by(name: kind)
  end
end
