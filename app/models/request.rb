# == Schema Information
#
# Table name: requests
#
#  id         :integer          not null, primary key
#  request    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact    :string
#

class Request < ActiveRecord::Base
  validates :contact, presence: true
end
