class SeoContent < ActiveRecord::Base
  validates :title, :meta_description, :address, :content, :meta_keywords, presence: true
end
