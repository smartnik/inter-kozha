Rails.application.routes.draw do

  mount Rich::Engine => '/rich', :as => 'rich'
  mount Ckeditor::Engine => '/ckeditor'
  ActiveAdmin.routes(self)
  root 'static_pages#index'

  #get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get :about, to: 'static_pages#about'
  get :contacts, to: 'static_pages#contacts'

  #resources :users
  resources :sessions
  resources :items
  resources :requests
  resources :email_subscriptions, only: :create
  resources :articles

  %w(404 422 500 503).each do |code|
    get code, to: "errors#show", code: code
  end
end
