class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.text :request
      t.string :email
      t.string :number

      t.timestamps null: false
    end
  end
end
