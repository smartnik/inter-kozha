class AddColorToItem < ActiveRecord::Migration
  def change
    add_column :items, :image, :string
    add_column :items, :color, :string
    add_column :items, :weight, :string
    add_column :items, :flag, :string
    add_column :items, :available, :boolean, default: :true
    add_column :items, :suitable, :string
  end
end
