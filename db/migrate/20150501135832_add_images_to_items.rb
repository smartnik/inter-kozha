class AddImagesToItems < ActiveRecord::Migration
  def change
    add_column :items, :images, :string, array: true
    remove_column :items, :image
  end
end
