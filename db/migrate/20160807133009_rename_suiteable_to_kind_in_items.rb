class RenameSuiteableToKindInItems < ActiveRecord::Migration
  def change
    rename_column :items, :suitable, :kind
  end
end
