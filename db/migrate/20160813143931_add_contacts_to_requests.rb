class AddContactsToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :contact, :string
    remove_column :requests, :email
    remove_column :requests, :number
  end
end
