class RemoveUnusedFieldsFromItems < ActiveRecord::Migration
  def change
    remove_column :items, :color
    remove_column :items, :weight
    remove_column :items, :available
    remove_column :items, :flag
    remove_column :items, :sizes
  end
end
