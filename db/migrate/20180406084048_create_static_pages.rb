class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.string :name, index: true
      t.text :text
      t.timestamps null: false
    end
  end
end
