class AddSizesToItems < ActiveRecord::Migration
  def change
    add_column :items, :sizes, :string
  end
end
