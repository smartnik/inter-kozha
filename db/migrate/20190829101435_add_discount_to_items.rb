class AddDiscountToItems < ActiveRecord::Migration
  def change
    add_column :items, :discount, :integer, default: 0, null: false
  end
end
