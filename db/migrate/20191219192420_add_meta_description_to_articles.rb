class AddMetaDescriptionToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :meta_description, :text
  end
end
