class CreateSeoContents < ActiveRecord::Migration
  def change
    create_table :seo_contents do |t|
      t.text :title
      t.text :meta_description
      t.text :content
      t.text :address, index: true

      t.timestamps null: false
    end
  end
end
