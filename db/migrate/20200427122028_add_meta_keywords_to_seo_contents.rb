class AddMetaKeywordsToSeoContents < ActiveRecord::Migration
  def change
    add_column :seo_contents, :meta_keywords, :string
  end
end
