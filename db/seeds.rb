# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

unless StaticPage.find_by(name: 'About Us')
  StaticPage.create(
    name: 'About Us',
    text: '<p> You can update this text at admin/static_pages. </p> <p> It <strong> supports </strong> html tags </p>'
  )
end

unless Category.first
  Category.create(name: 'Update at admin/categories')
end
